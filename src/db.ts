import Database from "better-sqlite3";
import { MemberInfo } from "oicq";
import * as c from "./constant";
import { CONV_NICK_LOOKUP_INFO } from "./interface";

export class AxonDatabase {
	db: Database.Database;
	id: number;

	constructor(path: string, self: number, opts: Database.Options) {
		this.db = new Database(path, opts);
		this.id = self
		/* 初始化数据库表 */
		this.db.exec(c.SQL_CREATE_TABLE);
		this.db.exec(c.SQL_DISABLE_SYNC);
	}

	insertMemberList(memberList: MemberInfo[]) {
		const db_insert = this.db.prepare(c.SQL_ACMAP_INSERT);

		this.db.transaction((members: MemberInfo[]) => {
			for (const m of members)
				db_insert.run({
					id: m.user_id,
					conv: m.group_id,
					nick: m.card === "" ? m.nickname : m.card,
				});
		})(memberList);

		this.fixDuplicate();
	}

	fixDuplicate() {
		const db_fix = this.db.prepare(c.SQL_ACMAP_FIX_DUP);
		db_fix.run({ ID: this.id });
	}

	fetchMemberName(ID: number, CONV: number): string {
		const db_search = this.db.prepare(c.SQL_ACMAP_SEARCH);
		return db_search.get({ ID, CONV })?.NICK;
	}

	deleteMember(ID: number, CONV: number) {
		const db_delete = this.db.prepare(c.SQL_ACMAP_DELETE);
		db_delete.run({ ID, CONV });
	}

	fetchMembers(CONV: number): Map<number, string> {
		const ret = new Map();
		const db_fetch = this.db.prepare(c.SQL_ACMAP_MEMBERS);

		db_fetch.all({ CONV }).forEach((x) => ret.set(x.ID, x.NICK));

		return ret;
	}

	lookupAcctId(NICK: string): number {
		let ret = 0;
		const db_fetch = this.db.prepare(c.SQL_ACMAP_LOOKUP_ID);

		db_fetch.all({ NICK }).forEach((x) => (ret = x.ID));

		return ret;
	}

	lookupAcctConvNick(ID: number): CONV_NICK_LOOKUP_INFO[] {
		const ret: CONV_NICK_LOOKUP_INFO[] = [];
		const db_fetch = this.db.prepare(c.SQL_ACMAP_LOOKUP_CONV_NICK);

		db_fetch
			.all({ ID })
			.forEach((x) => ret.push({ nick: x.NICK, conv: x.CONV }));

		return ret;
	}

	destory() {
		this.db.close();
	}
}
